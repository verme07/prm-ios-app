//
//  PoliceStationViewController.swift
//  PRM
//
//  Created by Johnny on 12/8/20.
//

import UIKit
import MapboxDirections
import MapboxCoreNavigation
import MapboxNavigation
import Kingfisher
import NVActivityIndicatorView

class PoliceStationViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
 
    var loader: NVActivityIndicatorView?
    
    fileprivate let application = UIApplication.shared
    //Stars
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star5: UIImageView!
    //End Stars
    
    func ratingStar(nr_star: Int) {
        if nr_star <= 1 {
            print("1 Star")
            star1.image = UIImage(named: "star")
            star2.image = UIImage(named: "star-outline")
            star3.image = UIImage(named: "star-outline")
            star4.image = UIImage(named: "star-outline")
            star5.image = UIImage(named: "star-outline")
        }
        if nr_star == 2 {
            print("2 Star")
            star1.image = UIImage(named: "star")
            star2.image = UIImage(named: "star")
            star3.image = UIImage(named: "star-outline")
            star4.image = UIImage(named: "star-outline")
            star5.image = UIImage(named: "star-outline")
        }
        if nr_star == 3 {
            print("3 Star")
            star1.image = UIImage(named: "star")
            star2.image = UIImage(named: "star")
            star3.image = UIImage(named: "star")
            star4.image = UIImage(named: "star-outline")
            star5.image = UIImage(named: "star-outline")
        }
        if nr_star == 4 {
            print("4 Star")
            star1.image = UIImage(named: "star")
            star2.image = UIImage(named: "star")
            star3.image = UIImage(named: "star")
            star4.image = UIImage(named: "star")
            star5.image = UIImage(named: "star-outline")
        }
        if nr_star >= 5 {
            print("5 Star")
            star1.image = UIImage(named: "star")
            star2.image = UIImage(named: "star")
            star3.image = UIImage(named: "star")
            star4.image = UIImage(named: "star")
            star5.image = UIImage(named: "star")
        }
    }
    //Main Buttons
    @IBOutlet weak var directionsBtn: UIView!
    
    @IBOutlet weak var denounceBtn: UIView!
    
    @IBOutlet weak var callBtn: UIView!
    
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var policeStationView: UIView!
    @IBOutlet weak var policeStationLabel: UILabel!
    
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var addressLabel: UILabel!
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var marks: UILabel!
    
    @IBOutlet weak var reviewStationBtn: UIButton!
    
    var mapView: NavigationMapView!
    
    var policeStation: PoliceStation?
    
    var latitude:Double?
    var longitude:Double?
    
    var photos = [UIImage(named: "default-thumbnail")!, UIImage(named: "default-thumbnail")!, UIImage(named: "default-thumbnail")!]
    
    var imagesArr:[String] = []
    var timer : Timer?
    var currentCellIndex = 0
    
    
    @IBAction func review(_ sender: Any) {
        if keyExist(key: "user_id") {
            performSegue(withIdentifier: "reviewPSSegue", sender: nil)
        }else{
            let alert = UIAlertController(title: "Login Necessario", message: "Para poder prosseguir voce precisa fazer o login", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Login", style: .default, handler: { action in
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "login", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "loginViewID") as! LoginViewController
                nextViewController.modalPresentationStyle = .fullScreen
           
                self.present(nextViewController, animated:true, completion:nil)
                
                
            }))
            alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)

        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loader = getLoader()
        imagesArr.append(policeStation?.photo1 ?? "default-thumbnail")
        imagesArr.append(policeStation?.photo2 ?? "default-thumbnail")
        imagesArr.append(policeStation?.photo3 ?? "default-thumbnail")
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        
        self.title = policeStation?.name
        pageControl.numberOfPages = imagesArr.count
      //  startTimer()
    
    
        let nr_star = Int(policeStation?.rating ?? 0)
        marks.text = "\(nr_star).0"
        ratingStar(nr_star: nr_star)
        
        phoneLabel.text = "\(policeStation!.phone1)"
        policeStationLabel.text = "\(policeStation!.phone2)"
        addressLabel.text = "\(policeStation!.address)"
        
        
        viewCustom(directionsBtn)
        viewCustom(denounceBtn)
        viewCustom(callBtn)
        
        btnCustom(reviewStationBtn)
        
        let gestureTap = UITapGestureRecognizer(target: self, action: #selector(self.getDirections(sender:)))
        directionsBtn.addGestureRecognizer(gestureTap)
        
        let gestureDenounce = UITapGestureRecognizer(target: self, action: #selector(self.getDenounce(sender:)))
        denounceBtn.addGestureRecognizer(gestureDenounce)
        
        let gestureCall = UITapGestureRecognizer(target: self, action: #selector(self.getCall(sender:)))
        callBtn.addGestureRecognizer(gestureCall)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
    }
    @objc func getDirections(sender: UITapGestureRecognizer) {
        loader?.startAnimating()
        displayDirections()
    }
    
    @objc func getDenounce(sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "denounceSegue", sender: nil)
    }
    
    @objc func getCall(sender: UITapGestureRecognizer) {
        makePhoneCall(phoneNumber: policeStation!.phone1)
    }
    
    func makePhoneCall(phoneNumber: String) {
        if let phoneURL = URL(string: ("tel://\(phoneNumber)")) {
            if application.canOpenURL(phoneURL) {
                application.open(phoneURL, options: [:], completionHandler: nil)
            }else{
                let alert = UIAlertController(title: "Erro!", message: "Não conseguimos ligar para esta esquadra/Posto policial", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func displayDirections() {
        let lat: CLLocationDegrees = latitude!
        let lng: CLLocationDegrees = longitude!
        
        let auxLat = Double(policeStation!.latitude)!
        let destinationLat: CLLocationDegrees = auxLat
        
        let auxLng = Double(policeStation!.longitude)!
        let destinationLng: CLLocationDegrees = auxLng
        
        // Define two waypoints to travel between
        let origin = Waypoint(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: lng), name: "Minha Localização")
        let destination = Waypoint(coordinate: CLLocationCoordinate2D(latitude: destinationLat, longitude: destinationLng), name: policeStation?.name)
        
        // Set options
        let routeOptions = NavigationRouteOptions(waypoints: [origin, destination])
        
        // Request a route using MapboxDirections.swift
        Directions.shared.calculate(routeOptions) { [weak self] (session, result) in
            switch result {
            case .failure(let error):
                print(error.localizedDescription)
                self?.loader?.stopAnimating()
            case .success(let response):
                guard let route = response.routes?.first, let strongSelf = self else {
                    return
                }
                // Pass the first generated route to the the NavigationViewController
                let viewController = NavigationViewController(for: route, routeIndex: 0, routeOptions: routeOptions)
                
                self?.loader?.stopAnimating()
                viewController.modalPresentationStyle = .fullScreen
                strongSelf.present(viewController, animated: true, completion: nil)
            }
        }
    }
    
    override func didMove(toParent parent: UIViewController?) {
        super.didMove(toParent: parent)
        if parent == nil {
       
        }
    }
    
    func mapView(_ mapView: MGLMapView, didFinishLoading style: MGLStyle) {
        if let mapView = mapView as? NavigationMapView {
            mapView.localizeLabels()
        }
    }
    
    func startTimer(){
        timer =  Timer.scheduledTimer(timeInterval: 2.5, target: self, selector: #selector(moveToNextIndex), userInfo: nil, repeats: true   )
    }
    
    @objc func moveToNextIndex(){
        if currentCellIndex < photos.count - 1 {
            currentCellIndex += 1
        }else{
            currentCellIndex = 0
        }
        collectionView.scrollToItem(at: IndexPath(item: currentCellIndex, section: 0), at: .centeredHorizontally, animated: true)
        pageControl.currentPage = currentCellIndex
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! ImageCollectionViewCell
        if imagesArr[indexPath.row] == "default-thumbnail" {
            cell.imgPoliceStation.image = UIImage(named: "default-thumbnail")
        }else{
            let imageName:String = (imagesArr[indexPath.row])
            let urlNew:String = imageName.replacingOccurrences(of: " ", with: "%20")
            let url = URL(string: "\(storageUrl)\(urlNew)")
            cell.imgPoliceStation.kf.indicatorType = .activity
            cell.imgPoliceStation.kf.setImage(with: url)
        }
  
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

        pageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }

    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {

        pageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "directionsSegue"){
            let directionsVC = segue.destination as! DirectionsViewController
            directionsVC.policeStation = policeStation
            directionsVC.lat = Double(latitude!)
            directionsVC.lng = Double(longitude!)
        }
        
        if(segue.identifier == "reviewPSSegue"){
            let reviewVC = segue.destination as! ReviewViewController
            reviewVC.policeStation = policeStation
        }
        
    }
}
