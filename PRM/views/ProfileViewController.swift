//
//  ProfileViewController.swift
//  PRM
//
//  Created by Johnny on 12/12/20.
//

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var editBtn: UIButton!
    
    @IBOutlet weak var exitBtn: UIButton!
    
    @IBOutlet weak var profileImgBtn: UIButton!
    
    @IBOutlet weak var userProfileImg: UIImageView!
    
    
    @IBOutlet weak var completeName: UILabel!
    
    @IBOutlet weak var email: UILabel!
    
    @IBOutlet weak var phone: UILabel!
    
    @IBOutlet weak var totalReceived: UILabel!
    
    @IBOutlet weak var totalSent: UILabel!
    //Login Section
    @IBOutlet weak var loginView: UIView!
    
    @IBOutlet weak var loginBtn: UIButton!
    
    
    func getValues()  {
        completeName.text = UserDefaults.standard.string(forKey: "user_name")
        email.text = UserDefaults.standard.string(forKey: "user_email")
        phone.text = UserDefaults.standard.string(forKey: "user_username")
    }
    
    @IBAction func logout(_ sender: Any) {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "user_id")
        defaults.removeObject(forKey: "user_email")
        defaults.removeObject(forKey: "user_state")
        defaults.removeObject(forKey: "user_name")
        defaults.removeObject(forKey: "user_username")
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "login", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "loginViewID") as! LoginViewController
        nextViewController.modalPresentationStyle = .fullScreen
        //                self.navigationController?.pushViewController(nextViewController, animated: true)
        self.present(nextViewController, animated:true, completion:nil)

    }
    
    @IBAction func login(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "login", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "loginViewID") as! LoginViewController
        nextViewController.modalPresentationStyle = .fullScreen
        //                self.navigationController?.pushViewController(nextViewController, animated: true)
        self.present(nextViewController, animated:true, completion:nil)
    }
    //End Login Section
    override func viewDidLoad() {
        super.viewDidLoad()
  
        btnCustom(editBtn)
        btnCustomOutline(exitBtn)
        btnCustom(loginBtn)
        makeRoundedImage(userProfileImg)
        profileImgBtn.layer.cornerRadius = 0.5 * profileImgBtn.bounds.size.width
        profileImgBtn.clipsToBounds = true

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getValues()
        if keyExist(key: "user_id") {
            loginView.isHidden = true
        
        }else{
            loginView.isHidden = false
        }
    }
    


}
