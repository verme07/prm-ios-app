//
//  StationsViewController.swift
//  PRM
//
//  Created by Johnny on 12/27/20.
//

import UIKit
import Alamofire
import MapboxDirections
import MapboxCoreNavigation
import MapboxNavigation
import NVActivityIndicatorView
import CoreLocation

class StationsViewController: UIViewController, UITableViewDataSource, UITabBarDelegate, UITableViewDelegate, UISearchBarDelegate, CLLocationManagerDelegate {
    
    fileprivate let application = UIApplication.shared
    
    @IBOutlet weak var searchBar: UISearchBar!
    var searching = false
    var searchSegue = false
    var textSegue = ""
    var loader: NVActivityIndicatorView?
    
    private var locationManager:CLLocationManager?
    var latitude:Float = -25.967
    var longitude:Float = 32.583
    
    @IBOutlet weak var stationsTableView: UITableView!
    
    var policeStations: [PoliceStation] = []
    
    var policeStationsSearch: [PoliceStation] = []
    
    var policeStation: PoliceStation?
    

    
    //MARK: UISearch
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        policeStationsSearch.removeAll()
            if searchText.isEmpty{
                searching = false
                stationsTableView.reloadData()
            }else{
                searching = true
            }
            var aux = ""
            for result in policeStations {
                aux = "\(result.name )"
                if aux.lowercased().contains(searchText.lowercased()) {
                    policeStationsSearch.append(result)
                }
                
                print(policeStationsSearch)
            }
        stationsTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        print("Event")
    }
    //MARK: Enf Of UISearch
    
    func getUserLocation() {
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager!.requestAlwaysAuthorization()
        locationManager!.startUpdatingLocation()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            latitude = Float(location.coordinate.latitude)
            longitude = Float(location.coordinate.longitude)
        }
    }
    
    @objc func getDirections(_ sender: UIButton){
        loader?.startAnimating()
        
        if searching {
            policeStation = policeStationsSearch[sender.tag]
            displayDirections()
        }else{
            policeStation = policeStations[sender.tag]
            displayDirections()
        }
    }
    
    @objc func getCall(_ sender: UIButton){
        
        if searching {
            policeStation = policeStationsSearch[sender.tag]
            makePhoneCall(phoneNumber: policeStation!.phone1)
        }else{
            policeStation = policeStations[sender.tag]
            makePhoneCall(phoneNumber: policeStation!.phone1)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching {
            return policeStationsSearch.count
        }else{
            return policeStations.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if searching {
            let cell = tableView.dequeueReusableCell(withIdentifier: "stationsIdentifier", for: indexPath) as! StationsTableViewCell
            cell.title.text = policeStationsSearch[indexPath.row].name
            cell.address.text = policeStationsSearch[indexPath.row].address
            viewCustom(cell.container)
            
            
            let lat: CLLocationDegrees = CLLocationDegrees(latitude)
            let lng: CLLocationDegrees = CLLocationDegrees(longitude)
            let coordinate0 = CLLocation(latitude: lat, longitude: lng)
            
            let markerLat: CLLocationDegrees = CLLocationDegrees(policeStationsSearch[indexPath.row].latitude)!
            let markerLng: CLLocationDegrees = CLLocationDegrees(policeStationsSearch[indexPath.row].longitude)!
            let coordinate1 = CLLocation(latitude: markerLat, longitude: markerLng)
            
            let distanceInMeters = coordinate0.distance(from: coordinate1)
            let kmDistance = distanceInMeters/1000
            cell.distance.text = "\(String(format: "%.1f", kmDistance)) Km"
            
            cell.directionsBtn.tag = indexPath.row
            cell.directionsBtn.addTarget(self, action: #selector(getDirections(_:)), for: .touchUpInside)
               
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "stationsIdentifier", for: indexPath) as! StationsTableViewCell
            cell.title.text = policeStations[indexPath.row].name
            cell.address.text = policeStations[indexPath.row].address
            viewCustom(cell.container)
            
            
            let lat: CLLocationDegrees = CLLocationDegrees(latitude)
            let lng: CLLocationDegrees = CLLocationDegrees(longitude)
            let coordinate0 = CLLocation(latitude: lat, longitude: lng)
            
            let markerLat: CLLocationDegrees = CLLocationDegrees(policeStations[indexPath.row].latitude)!
            let markerLng: CLLocationDegrees = CLLocationDegrees(policeStations[indexPath.row].longitude)!
            let coordinate1 = CLLocation(latitude: markerLat, longitude: markerLng)
            
            let distanceInMeters = coordinate0.distance(from: coordinate1)
            let kmDistance = distanceInMeters/1000
            cell.distance.text = "\(String(format: "%.1f", kmDistance)) Km"
            
            cell.directionsBtn.tag = indexPath.row
            cell.directionsBtn.addTarget(self, action: #selector(getDirections(_:)), for: .touchUpInside)
            
            cell.callBtn.tag = indexPath.row
            cell.callBtn.addTarget(self, action: #selector(getCall(_:)), for: .touchUpInside)
            return cell
        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if searching {
            stationsTableView.deselectRow(at: indexPath, animated: true)
            policeStation = policeStationsSearch[indexPath.row]
            performSegue(withIdentifier: "viewPSSegue", sender: nil)
        }else{
            stationsTableView.deselectRow(at: indexPath, animated: true)
            policeStation = policeStations[indexPath.row]
            performSegue(withIdentifier: "viewPSSegue", sender: nil)
        }
  


    }

    override func viewDidLoad() {
        super.viewDidLoad()
        getUserLocation()
        loader = getLoader()
        searchBar.delegate = self
        self.stationsTableView.delegate = self
        self.stationsTableView.dataSource = self
        self.stationsTableView.tableFooterView = UIView()
    
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
        if searchSegue {
            searchBar.becomeFirstResponder()
            searchBar.text = textSegue
            var aux = ""
            for result in policeStations {
                aux = "\(result.name )"
                if aux.lowercased().contains(textSegue.lowercased()) {
                    policeStationsSearch.append(result)
                }
                
                print(policeStationsSearch)
            }
            searching = true
        }
    
        if policeStations.count <= 0 {
            getPoliceStations()
        }
    }
    
    func getPoliceStations() {
        self.loader?.startAnimating()
        let headers: HTTPHeaders = [
            "Accept": "application/json",
        ]
        //Send a request with Alamofire
        AF.request("\(host)/all-police-stations", method: .get, headers: headers).responseJSON{ [self]response in
            if response.response?.statusCode == 200{
                if let json = response.data {
                    do {
                        let decoder = JSONDecoder()
                        self.policeStations = try decoder.decode([PoliceStation].self, from: json)
                        
                        stationsTableView.reloadData()
                        self.loader?.stopAnimating()
                      
                    } catch let error {
                        let alert = UIAlertController(title: "Erro!", message: "Verifique sua conexão a internet ou volte a tentar mais tarde", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        loader?.stopAnimating()
                        print(error)
                    }
                }
                
            }else{
                let alert = UIAlertController(title: "Erro!", message: "Verifique sua conexão a internet ou volte a tentar mais tarde", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                loader?.stopAnimating()
    
            }
        }
    }
    
    func makePhoneCall(phoneNumber: String) {
        if let phoneURL = URL(string: ("tel://\(phoneNumber)")) {
            if application.canOpenURL(phoneURL) {
                application.open(phoneURL, options: [:], completionHandler: nil)
            }else{
                let alert = UIAlertController(title: "Erro!", message: "Não conseguimos ligar para esta esquadra/Posto policial", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func displayDirections() {
        let lat: CLLocationDegrees = CLLocationDegrees(latitude)
        let lng: CLLocationDegrees = CLLocationDegrees(longitude)
        
        let auxLat = Double(policeStation!.latitude)!
        let destinationLat: CLLocationDegrees = auxLat
        
        let auxLng = Double(policeStation!.longitude)!
        let destinationLng: CLLocationDegrees = auxLng
        
        // Define two waypoints to travel between
        let origin = Waypoint(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: lng), name: "Minha Localização")
        let destination = Waypoint(coordinate: CLLocationCoordinate2D(latitude: destinationLat, longitude: destinationLng), name: policeStation?.name)
        
        // Set options
        let routeOptions = NavigationRouteOptions(waypoints: [origin, destination])
        
        // Request a route using MapboxDirections.swift
        Directions.shared.calculate(routeOptions) { [weak self] (session, result) in
            switch result {
            case .failure(let error):
                print(error.localizedDescription)
                self?.loader?.stopAnimating()
            case .success(let response):
                guard let route = response.routes?.first, let strongSelf = self else {
                    return
                }
                // Pass the first generated route to the the NavigationViewController
                let viewController = NavigationViewController(for: route, routeIndex: 0, routeOptions: routeOptions)
                
                self?.loader?.stopAnimating()
                viewController.modalPresentationStyle = .fullScreen
                strongSelf.present(viewController, animated: true, completion: nil)
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "viewPSSegue"){
            let policeStationVC = segue.destination as! PoliceStationViewController
            policeStationVC.policeStation = policeStation
            policeStationVC.latitude = Double(latitude)
            policeStationVC.longitude = Double(longitude)
        }
    }
    

}
