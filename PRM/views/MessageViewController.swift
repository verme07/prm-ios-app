//
//  MessageViewController.swift
//  PRM
//
//  Created by Johnny on 1/6/21.
//

import UIKit

class MessageViewController: UIViewController {
    
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var author: UILabel!
    
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var message: UITextView!
    
    @IBOutlet weak var userImg: UIImageView!
    
    var messageTxt = ""
    var authorTxt = ""
    var dateTxt = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        image.layer.borderWidth = 0.1
        image.layer.masksToBounds = false
       
        image.layer.cornerRadius = 5
        image.clipsToBounds = true
        author.text = authorTxt
        
        message.text = messageTxt
        
        let stringDate = dateTxt // ISO 8601 format
        
        let rfc3339DateFormatter = DateFormatter()
        rfc3339DateFormatter.locale = Locale(identifier: "en_US_POSIX")
        rfc3339DateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSS'Z'"
        rfc3339DateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        let dateF = rfc3339DateFormatter.date(from: stringDate)
        
        let dateFormatter = DateFormatter()
        
        // Set Date Format
        dateFormatter.dateFormat = "dd/MM/YYYY"
        
        // Convert Date to String
        date.text = dateFormatter.string(from: dateF!)
    }
    
    
    
    
}
