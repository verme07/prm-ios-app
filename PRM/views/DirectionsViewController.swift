//
//  DirectionsViewController.swift
//  PRM
//
//  Created by Johnny on 12/17/20.
//

import UIKit

import MapboxDirections
import MapboxCoreNavigation
import MapboxNavigation

class DirectionsViewController: UIViewController, NavigationServiceDelegate, MGLMapViewDelegate {
    
    @IBOutlet weak var mapView: NavigationMapView!
    
    var policeStation: PoliceStation?
    
    var lat:Double?
    var lng:Double?

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let latitude: CLLocationDegrees = lat!
        let longitude: CLLocationDegrees = lng!
        
        let auxLat = Double(policeStation!.latitude)!
        let destinationLat: CLLocationDegrees = auxLat
        
        let auxLng = Double(policeStation!.longitude)!
        let destinationLng: CLLocationDegrees = auxLng
        
        // Define two waypoints to travel between
        let origin = Waypoint(coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude), name: "Minha Localização")
        let destination = Waypoint(coordinate: CLLocationCoordinate2D(latitude: destinationLat, longitude: destinationLng), name: policeStation?.name)
        
        // Set options
        let routeOptions = NavigationRouteOptions(waypoints: [origin, destination])
        
        // Request a route using MapboxDirections.swift
        Directions.shared.calculate(routeOptions) { [weak self] (session, result) in
            switch result {
            case .failure(let error):
                print(error.localizedDescription)
            case .success(let response):
                guard let route = response.routes?.first, let strongSelf = self else {
                    return
                }
                // Pass the first generated route to the the NavigationViewController
                let viewController = NavigationViewController(for: route, routeIndex: 0, routeOptions: routeOptions)
                
                viewController.modalPresentationStyle = .currentContext
                strongSelf.present(viewController, animated: true, completion: nil)
            }
        }
    }
    
    override func didMove(toParent parent: UIViewController?) {
        super.didMove(toParent: parent)
        if parent == nil {
       
        }
    }

    func navigationViewControllerDidCancelNavigation(_ navigationViewController: NavigationViewController) {
 
        navigationViewController.dismiss(animated: true, completion: nil)
    }
    func mapView(_ mapView: MGLMapView, didFinishLoading style: MGLStyle) {
        if let mapView = mapView as? NavigationMapView {
            mapView.localizeLabels()
        }
    }
    
}
