//
//  MapViewController.swift
//  PRM
//
//  Created by Johnny on 12/8/20.
//

import UIKit
import GoogleMaps
import CoreLocation
import Alamofire
import NVActivityIndicatorView
import IQKeyboardManagerSwift


class MapViewController: UIViewController, CLLocationManagerDelegate, UISearchBarDelegate {
    
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var loader: NVActivityIndicatorView?
    
    let markerImage = UIImage(named: "esquadra_marker")!
    
    
    @IBOutlet weak var mapView: GMSMapView!
    
    private var locationManager:CLLocationManager?
    var policeStation: PoliceStation?
    
    var policeStations: [PoliceStation] = []
    var latitude:Float = -25.967
    var longitude:Float = 32.583
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
       
            if searchText.isEmpty{
               
            }else{
                performSegue(withIdentifier: "searchSegue", sender: nil)
            }
    
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        print("Event")
    }
    
    func getUserLocation() {
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager!.requestAlwaysAuthorization()
        locationManager!.startUpdatingLocation()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            latitude = Float(location.coordinate.latitude)
            longitude = Float(location.coordinate.longitude)
        }
    }
    
    private func setupMap(){
      
        mapView?.delegate = self
        mapView.settings.compassButton = true
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
      
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude), zoom: 14.0)
        mapView.camera = camera
        
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        loader = getLoader()
        
        searchBar.delegate = self
        print("Key exist: \(keyExist(key: "user_id"))")
        getUserLocation()
        setupMap()
        getMarkers()
        tabBarController?.tabBar.unselectedItemTintColor = UIColor.gray
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
    }
    
    func createMarkers() {
        
        var lat = 0.0
        var lgn = 0.0
        var i = 0
        for station in policeStations {
            var marker = GMSMarker()
            lat = Double(station.latitude)!
            lgn = Double(station.longitude)!
            marker.position = CLLocationCoordinate2D(latitude: lat, longitude: lgn)
            marker.title = "\(i)"
            i += 1
            marker.snippet = station.address
      
            //creating a marker view
            marker.icon = markerImage
            marker.map = mapView
        }
    }
    
    func getMarkers() {
        loader?.startAnimating()
        let headers: HTTPHeaders = [
            "Accept": "application/json",
        ]
        //Send a request with Alamofire
        AF.request("\(host)/all-police-stations", method: .get, headers: headers).responseJSON{ [self]response in
            if response.response?.statusCode == 200{
                if let json = response.data {
                    do {
                        let decoder = JSONDecoder()
                        self.policeStations = try decoder.decode([PoliceStation].self, from: json)
                        //                        print("Response: \(self.policeStations)")
                        self.createMarkers()
                        self.loader?.stopAnimating()
                    } catch let error {
                        
                        let alert = UIAlertController(title: "Erro!", message: "Verifique sua conexão a internet ou volte a tentar mais tarde", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        loader?.stopAnimating()
                        print(error)
                    }
                }
                
            }else{
                let alert = UIAlertController(title: "Erro!", message: "Verifique sua conexão a internet ou volte a tentar mais tarde", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                loader?.stopAnimating()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "viewDetailsSegue"){
            let policeStationVC = segue.destination as! PoliceStationViewController
            policeStationVC.policeStation = policeStation
            policeStationVC.latitude = Double(latitude)
            policeStationVC.longitude = Double(longitude)
        }
        
        if(segue.identifier == "searchSegue"){
            let data = segue.destination as! StationsViewController
            data.searchSegue =  true
            data.textSegue = searchBar.text!
            data.policeStations = policeStations
            
            searchBar.text! = ""
        }
    }
    
}
extension MapViewController: GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        let view = Bundle.main.loadNibNamed("MarkerWindow", owner: self, options: nil)![0] as! MarkerWindow
        let sizeWith = view.frame.width * 0.6
        let sizeHeight = view.frame.height * 0.6
        
        let indexItem = Int(marker.title!)
        let markerItem = policeStations[indexItem!]
        
        let lat: CLLocationDegrees = CLLocationDegrees(latitude)
        let lng: CLLocationDegrees = CLLocationDegrees(longitude)
        let coordinate0 = CLLocation(latitude: lat, longitude: lng)
        
        let markerLat: CLLocationDegrees = CLLocationDegrees(markerItem.latitude)!
        let markerLng: CLLocationDegrees = CLLocationDegrees(markerItem.longitude)!
        let coordinate1 = CLLocation(latitude: markerLat, longitude: markerLng)
        
        let distanceInMeters = coordinate0.distance(from: coordinate1)
        
        view.layer.cornerRadius = 10
        view.btnReport.layer.cornerRadius = 4
        view.btnReport.clipsToBounds = true
        
        view.address.text = markerItem.address
        view.contact.text = markerItem.phone1
        view.name.text = markerItem.name
        let kmDistance = distanceInMeters/1000
        view.distance.text = "\(String(format: "%.1f", kmDistance)) Km"
        
        let frame = CGRect(x: 20, y: 20, width: sizeWith, height: sizeHeight)
        view.frame = frame
        
 
        return view
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        let indexItem = Int(marker.title!)
        
        policeStation = policeStations[indexItem!]
        print("marker Window \(indexItem!)")
        self.performSegue(withIdentifier: "viewDetailsSegue", sender: nil)

    }
}
