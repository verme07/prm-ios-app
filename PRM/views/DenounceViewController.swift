//
//  DenounceViewController.swift
//  PRM
//
//  Created by Johnny on 12/12/20.
//

import UIKit
import iOSDropDown
import CoreLocation
import Alamofire
import NVActivityIndicatorView
import IQKeyboardManagerSwift

class DenounceViewController: UIViewController, CLLocationManagerDelegate{
    
    var loader: NVActivityIndicatorView?
    
    var userID: Int?
    
    @IBOutlet weak var policeStationList: DropDown!
    
    @IBOutlet weak var loginView: UIView!
    
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBAction func login(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "login", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "loginViewID") as! LoginViewController
        nextViewController.modalPresentationStyle = .fullScreen
        //                self.navigationController?.pushViewController(nextViewController, animated: true)
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    
    
    var policeStations: [PoliceStation] = []
    var listPoliceStations:[String] = []
    
    var selectedPS:Int?
    
    
    @IBOutlet weak var locationSwitch: UISwitch!
    
    
    private var locationManager:CLLocationManager?
    var latitude:Float = -25.967
    var longitude:Float = 32.583
    
    func getUserLocation() {
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager!.requestAlwaysAuthorization()
        locationManager!.startUpdatingLocation()
        
        
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            latitude = Float(location.coordinate.latitude)
            longitude = Float(location.coordinate.longitude)
            print("Latitude: \(latitude)")
        }
    }
    
    @IBOutlet weak var imageDenounce: UIImageView!
    
    @IBOutlet weak var imgUploadBtn: UIButton!
    
    @IBOutlet weak var sendBtn: UIButton!
    
    @IBOutlet weak var denounceText: UITextView!
    
    let stations = ["Estadio", "6a Esquadra"]
    
    var pickerView = UIPickerView();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userID = UserDefaults.standard.integer(forKey: "user_id")
        loader = getLoader()
        getPoliceStations()

       
        getUserLocation()
//        policeStation.inputView = pickerView
        
        btnCustom(imgUploadBtn)
        btnCustom(sendBtn)
        btnCustom(loginBtn)
        
        let borderGray = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
          self.denounceText.layer.borderColor = borderGray.cgColor
          self.denounceText.layer.borderWidth = 0.7
          self.denounceText.layer.cornerRadius = 5
        
        imageDenounce.layer.cornerRadius = 5
        imageDenounce.layer.masksToBounds = true
        
        policeStationList.didSelect{(selectedText , index ,id) in
        print("Selected String: \(selectedText) \n index: \(index)")
            self.selectedPS = index
        }
        // Do any additional setup after loading the view.
        
   
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
  
        if keyExist(key: "user_id") {
            loginView.isHidden = true
        
        }else{
            loginView.isHidden = false
        }
    }
    
    @IBAction func sendData(_ sender: Any) {
        if validade() {
            sendDenunce()
        }
      
        
    }
    
    func validade() -> Bool {
        
        if policeStationList.text == "" {
            let alert = UIAlertController(title: "Selecione um posto policial", message: "Selecione o posto policial que pretende fazer a denuncia", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            return false;
        }
        if denounceText.text == "" {
            let alert = UIAlertController(title: "Sem Denuncia", message: "Campo de denuncia vazio. Por favor  escreva a sua denuncia", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            return false
        }
        
        return true
    }
    
    func sendDenunce() {
        
        loader?.startAnimating()

        let body = [
            "chat_name": "Teste",
            "police_station_id": self.policeStations[selectedPS!].id,
            "user_id": userID!,
            "message": denounceText.text!,
            "sender": 0,
            "latitude": latitude,
            "longitude": longitude
            ] as [String : Any]
        
        print(body)
        let headers: HTTPHeaders = [
            "Accept": "application/json",
        ]
        
        AF.request("\(host)/sent-message", method: .post,  parameters: body, headers: headers).responseJSON{ [self]response in
            if response.response?.statusCode == 200{
                if let json = response.data {
                    do {
                        let decoder = JSONDecoder()
                        
                        let success = try decoder.decode(DenounceSuccess.self, from: json)
                        
                        let alert = UIAlertController(title: "Sucesso!", message: "Sua denuncia foi enviada com sucesso por favor aguarde por uma resposta", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        policeStationList.text = ""
                        denounceText.text = ""
                        
                        loader?.stopAnimating()
                    } catch let error {
                        let alert = UIAlertController(title: "Erro!", message: "Verifique sua conexão a internet ou volte a tentar mais tarde", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        loader?.stopAnimating()
                        print(error)
                    }
                }
                
            }else{
                let alert = UIAlertController(title: "Erro!", message: "Verifique sua conexão a internet ou volte a tentar mais tarde", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                loader?.stopAnimating()
            }
        }
        
    }

    
    func getPoliceStations() {
        loader?.startAnimating()
        let headers: HTTPHeaders = [
            "Accept": "application/json",
        ]
        //Send a request with Alamofire
        AF.request("\(host)/all-police-stations", method: .get, headers: headers).responseJSON{ [self]response in
            if response.response?.statusCode == 200{
                if let json = response.data {
                    do {
                        let decoder = JSONDecoder()
                        self.policeStations = try decoder.decode([PoliceStation].self, from: json)
                        
                        for station in self.policeStations {
                            listPoliceStations.append(station.name)
                        }
                        // print(listPoliceStations)
                        policeStationList.optionArray = listPoliceStations
                        
                        loader?.stopAnimating()
                    
                    } catch let error {
                        let alert = UIAlertController(title: "Erro!", message: "Verifique sua conexão a internet ou volte a tentar mais tarde", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        loader?.stopAnimating()
                        print(error)
                    }
                }
                
            }else{
                let alert = UIAlertController(title: "Erro!", message: "Verifique sua conexão a internet ou volte a tentar mais tarde", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                loader?.stopAnimating()
            }
        }
    }
    
    @IBAction func setImage(_ sender: Any) {
        let imagePickerVC = UIImagePickerController()
           imagePickerVC.sourceType = .photoLibrary
           present(imagePickerVC, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)

        if let image = info[.originalImage] as? UIImage {
            self.imageDenounce.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        }
    }
    
}
