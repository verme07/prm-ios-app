//
//  InboxViewController.swift
//  PRM
//
//  Created by Johnny on 12/10/20.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class InboxViewController: UIViewController, UITabBarDelegate, UITableViewDataSource, UITableViewDelegate {
    
    var loader: NVActivityIndicatorView?
    
    
    @IBOutlet weak var inboxTable: UITableView!
    
    @IBOutlet weak var loginView: UIView!
    
    @IBOutlet weak var loginBtn: UIButton!
    

    var userID: Int?
    
    var sentMessage: Message?
    var receivedMessage: Message?
    
    @IBAction func login(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "login", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "loginViewID") as! LoginViewController
        nextViewController.modalPresentationStyle = .fullScreen
        //                self.navigationController?.pushViewController(nextViewController, animated: true)
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var messagesSent: [Message] = []
    var messagesReceived: [Message] = []
    
    var switchValue = 0
    @IBAction func inboxType(_ sender: Any) {
        
        switch segmentedControl.selectedSegmentIndex
            {
            case 0:
                switchValue = 0
                if messagesSent.count > 0 {
                    inboxTable.reloadData()
                }else{
                    getSentMessages()
                }
                
            case 1:
                switchValue = 1
                if messagesReceived.count > 0 {
                    inboxTable.reloadData()
                }else{
                    getReceivedMessages()
                }
            default:
                break
            }
    }
 
    override func viewDidLoad() {
        super.viewDidLoad()
        userID = UserDefaults.standard.integer(forKey: "user_id")
        loader = getLoader()
        inboxTable.tableFooterView = UIView()
        getSentMessages()
        
        btnCustom(loginBtn)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if messagesSent.count <= 0 {
            getSentMessages()
        }
        if messagesReceived.count <= 0 {
            getReceivedMessages()
        }
        
        if keyExist(key: "user_id") {
            loginView.isHidden = true
        
        }else{
            loginView.isHidden = false
        }
    }
    
    func getSentMessages() {
        
        loader?.startAnimating()
        let headers: HTTPHeaders = [
            "Accept": "application/json",
        ]
        //Send a request with Alamofire
        AF.request("\(host)/get-sent-messages/\(userID ?? 0)", method: .get, headers: headers).responseJSON{ [self]response in
            if response.response?.statusCode == 200{
                if let json = response.data {
                    do {
                        let decoder = JSONDecoder()
                        self.messagesSent = try decoder.decode([Message].self, from: json)
                        
                        inboxTable.reloadData()
                        self.loader?.stopAnimating()
                    } catch let error {
                        print(error)
                        self.loader?.stopAnimating()
                    }
                }
            }else{
                print("Error")
                self.loader?.stopAnimating()
            }
        }
    }
    
    func getReceivedMessages() {
        self.loader?.startAnimating()
        let headers: HTTPHeaders = [
            "Accept": "application/json",
        ]
        //Send a request with Alamofire
        AF.request("\(host)/get-received-messages/\(userID ?? 0)", method: .get, headers: headers).responseJSON{ [self]response in
            if response.response?.statusCode == 200{
                if let json = response.data {
                    do {
                        let decoder = JSONDecoder()
                        self.messagesReceived = try decoder.decode([Message].self, from: json)
                        
                        inboxTable.reloadData()
                        self.loader?.stopAnimating()
                    } catch let error {
                        let alert = UIAlertController(title: "Erro!", message: "Verifique sua conexão a internet ou volte a tentar mais tarde", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        loader?.stopAnimating()
                        print(error)
                    }
                }
            }else{
                let alert = UIAlertController(title: "Erro!", message: "Verifique sua conexão a internet ou volte a tentar mais tarde", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                loader?.stopAnimating()
    
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if switchValue == 0 {
            return messagesSent.count
        }else if switchValue == 1 {
            return messagesReceived.count
        }
        
        return messagesSent.count
   
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as! InboxTableViewCell
        if switchValue == 0 {
            cell.title.text = messagesSent[indexPath.row].station.name
            cell.message.text = messagesSent[indexPath.row].message
            return cell
        }else if switchValue == 1 {
            cell.title.text = messagesReceived[indexPath.row].station.name
            cell.message.text = messagesReceived[indexPath.row].message
            return cell
        }
        
        return cell
 
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if switchValue == 0 {
            inboxTable.deselectRow(at: indexPath, animated: true)
            sentMessage = messagesSent[indexPath.row]
            performSegue(withIdentifier: "messageSegue", sender: nil)
        }else if switchValue == 1 {
            inboxTable.deselectRow(at: indexPath, animated: true)
            receivedMessage = messagesReceived[indexPath.row]
            performSegue(withIdentifier: "messageSegue", sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "messageSegue"){
            
            if switchValue == 0 {
                let messageVc = segue.destination as! MessageViewController
                messageVc.messageTxt = sentMessage!.message
                messageVc.authorTxt = "Mesagem Enviada"
                messageVc.dateTxt = sentMessage!.created_at
            }else if switchValue == 1 {
                let messageVc = segue.destination as! MessageViewController
                messageVc.messageTxt = receivedMessage!.message
                messageVc.authorTxt = "Mesagem Recebida"
                messageVc.dateTxt = receivedMessage!.created_at
            }
       
     
        }
    }
    
}
