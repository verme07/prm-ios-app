//
//  ReviewViewController.swift
//  PRM
//
//  Created by Johnny on 12/9/20.
//

import UIKit
import AARatingBar
import Alamofire
import NVActivityIndicatorView

class ReviewViewController: UIViewController {

    var userID: Int?
    var loader: NVActivityIndicatorView?
    
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var sendBtn: UIButton!
    
    @IBOutlet weak var rating: AARatingBar!
    
    var policeStation: PoliceStation?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader = getLoader()
        userID = UserDefaults.standard.integer(forKey: "user_id")
        let borderGray = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
          self.textView.layer.borderColor = borderGray.cgColor
          self.textView.layer.borderWidth = 0.7
          self.textView.layer.cornerRadius = 5
        
        btnCustom(sendBtn)
    }
    
    @IBAction func sendReview(_ sender: Any) {
        sendReview()
    }
    
    
    func sendReview() {
        self.loader?.startAnimating()
        let body = [
            "police_station_id": policeStation?.id,
            "user_id": userID!,
            "review": textView.text!,
            "rating": rating.value,
       
            ] as [String : Any]
        
        print(body)
        let headers: HTTPHeaders = [
            "Accept": "application/json",
        ]
    
    
        AF.request("\(host)/add-review", method: .post,  parameters: body, headers: headers).responseJSON{ [self]response in
            if response.response?.statusCode == 200{
                if let json = response.data {
                    do {
                        let decoder = JSONDecoder()
                        
                        let success = try decoder.decode(ReviewSuccess.self, from: json)
                        
                        let alert = UIAlertController(title: "Sucesso!", message: "Sua avaliação foi enviada com sucesso por favor aguarde por uma resposta", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        self.loader?.stopAnimating()
                        
                        textView.text = ""
                        rating.value = 0
                        
                    
                    } catch let error {
                        self.loader?.stopAnimating()
                        let alert = UIAlertController(title: "Erro!", message: "Ocorreu um erro tente novamente mais tarde ou verifique sua conexão a internet", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        print(error)
                        
                    }
                }
                
            }else{
                self.loader?.stopAnimating()
                let alert = UIAlertController(title: "Erro!", message: "Ocorreu um erro tente novamente mais tarde ou verifique sua conexão a internet", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                print(response)
            }
        }
        
    }
}
