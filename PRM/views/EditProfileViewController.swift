//
//  EditProfileViewController.swift
//  PRM
//
//  Created by Johnny on 12/28/20.
//

import UIKit
import Eureka
import NVActivityIndicatorView
import Alamofire
import IQKeyboardManagerSwift

class EditProfileViewController: FormViewController {

    var loader: NVActivityIndicatorView?
    
    var userID:Int = 0
    
    var name: String = ""
    var phone: String = ""
    var email: String = ""
    var password: String = ""
    let defaults = UserDefaults.standard
    
    func getValues()  {
        name = UserDefaults.standard.string(forKey: "user_name")!
        email = UserDefaults.standard.string(forKey: "user_email")!
        phone = UserDefaults.standard.string(forKey: "user_username")!
        userID = UserDefaults.standard.integer(forKey: "user_id")
    }
    
    func saveUser(user: User) {
        defaults.set(user.id, forKey: "user_id")
        defaults.set(user.email, forKey: "user_email")
        defaults.set(user.state, forKey: "user_state")
        defaults.set(user.name, forKey: "user_name")
        defaults.set(user.username, forKey: "user_username")
        
        print("Save User \(defaults.integer(forKey: "user_id"))")
    }
    
    
    func updateUser() {
        
        loader?.startAnimating()
        let body = [
            "name": name,
            "phone": phone,
            "email": email,
            "password": password,
            "user_id": userID
            ] as [String : Any]
        
        let headers: HTTPHeaders = [
            "Accept": "application/json",
        ]
    
    
        AF.request("\(host)/edit-user-ios", method: .post,  parameters: body, headers: headers).responseJSON{ [self]response in
            if response.response?.statusCode == 200{
                if let json = response.data {
                    do {
                        let decoder = JSONDecoder()
                        
                        let success = try decoder.decode(User.self, from: json)
                        
                        saveUser(user: success)
                        let alert = UIAlertController(title: "Sucesso!", message: "Dados actualizados com sucesso!", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        loader?.stopAnimating()
                        
                    } catch let error {
                        let alert = UIAlertController(title: "Erro!", message: "Verifique sua conexão a internet ou volte a tentar mais tarde", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        loader?.stopAnimating()
                        print(error)
                    }
                }
                
            }else{
                let alert = UIAlertController(title: "Erro!", message: "Verifique sua conexão a internet ou volte a tentar mais tarde", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                loader?.stopAnimating()
            }
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        getValues()
        form +++ Section("Editar Dodos do Utilizador")
            <<< TextRow(){
                $0.title = "Nome"
                $0.tag = "nome"
                $0.placeholder = "Nome Completo"
                $0.value = name
                $0.add(rule: RuleRequired(msg: "Insira o Nome"))
                
                $0.validationOptions = .validatesOnChange

            }.cellSetup { cell, row in
                cell.tintColor = .orange
            }
            .cellUpdate { cell, row in
                if !row.isValid {
                    cell.titleLabel?.textColor = .systemRed
                }
                self.name = row.value ?? ""
            }
            <<< EmailRow(){
                $0.title = "Email"
                $0.placeholder = "Email"
                $0.value = email
                $0.add(rule: RuleRequired(msg: "Insira o Nome"))
                $0.validationOptions = .validatesOnChange
            }.cellSetup { cell, row in
                cell.tintColor = .orange
            }
            .cellUpdate { cell, row in
                if !row.isValid {
                    cell.titleLabel?.textColor = .systemRed
                }
                self.email = row.value ?? ""
            }
            <<< PhoneRow(){
                $0.title = "Celular"
                $0.value = phone
                $0.placeholder = "Numero de Celular"
                $0.add(rule: RuleRequired(msg: "Insira o Nome"))
                $0.validationOptions = .validatesOnChange
            }.cellSetup { cell, row in
                cell.tintColor = .orange
            }
            .cellUpdate { cell, row in
                if !row.isValid {
                    cell.titleLabel?.textColor = .systemRed
                }
                self.phone = row.value ?? ""
            }
            
            +++ Section("Editar Password")
            
            <<< PasswordRow("Senha"){
                $0.title = "Senha"
                $0.add(rule: RuleRequired(msg: "Insira o Nome"))
                
                $0.validationOptions = .validatesOnChange
            }.cellSetup { cell, row in
                cell.tintColor = .orange
            }
            .cellUpdate { cell, row in
                if !row.isValid {
                    cell.titleLabel?.textColor = .systemRed
                }
                
                self.password = row.value ?? ""
            }
       
            <<< ButtonRow() { (row: ButtonRow) -> Void in
                row.title = "Editar"
            }.cellSetup { cell, row in
                cell.backgroundColor = UIColor(named: "color_primary")
                cell.tintColor = .white
                
            }
            .onCellSelection { [weak self] (cell, row) in
//                print("validating errors: \(row.section?.form?.validate().count)")
                if row.section?.form?.validate().count == 0{
    
                    self!.updateUser()
                }
            }
        // Do any additional setup after loading the view.
    }

}
