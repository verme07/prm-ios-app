//
//  User.swift
//  PRM
//
//  Created by Johnny on 12/26/20.
//

import Foundation

struct User: Codable {
    let id: Int
    let name, username: String
    let type, state: Int?
    let profile_img, email: String?
    let police_station_id: Int?
    let email_verified_at: String?
    let created_at, updated_at: String
}
