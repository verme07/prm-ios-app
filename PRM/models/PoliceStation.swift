//
//  PoliceStation.swift
//  PRM
//
//  Created by Johnny on 12/14/20.
//

import Foundation
struct PoliceStation: Codable {
    let id: Int
    let name: String
    let province, city: String
    let address, email: String
    let photo: String?
    let phone1, phone2, telephone: String
    let latitude: String
    let longitude, description: String
    let photo1, photo2, photo3: String?
    let rating: Double
    let nr_reviews: Int
    let created_at, updated_at: String


}
