//
//  DenounceSuccess.swift
//  PRM
//
//  Created by Johnny on 12/26/20.
//

import Foundation

struct DenounceSuccess: Codable {
    let message, police_station_id, user_id: String
    let conversation_id: Int
    let latitude, longitude, updated_at, created_at: String
    let id: Int

}
