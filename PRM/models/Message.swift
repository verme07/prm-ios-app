//
//  Message.swift
//  PRM
//
//  Created by Johnny on 12/26/20.
//

import Foundation

struct Message: Codable {
    let id: Int
    let message: String
    let type, sender: Int
    let media: String?
    let police_station_id, user_id: Int
    let latitude, longitude: String?
    let conversation_id: Int
    let created_at, updated_at: String
    let station: Station
    let user: UserMini
}

struct Station: Codable {
    let id: Int
    let name: String
}

struct UserMini: Codable {
    let id: Int
    let name: String
}
