//
//  ReviewSuccess.swift
//  PRM
//
//  Created by Johnny on 12/29/20.
//

import Foundation
// MARK: - ReviewSuccess
struct ReviewSuccess: Codable {
    let review, rating, police_station_id, user_id: String
    let created_at, updated_at: String
    let id: Int
}
