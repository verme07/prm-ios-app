//
//  LoginViewController.swift
//  PRM
//
//  Created by Johnny on 12/13/20.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import IQKeyboardManagerSwift

class LoginViewController: UIViewController {

    
    var loader: NVActivityIndicatorView?
    
    @IBOutlet weak var phoneTxt: UITextField!{
        didSet {
            phoneTxt.tintColor = UIColor.lightGray
            phoneTxt.setIcon(UIImage(named: "phone")!)
        }
     }
    
    @IBOutlet weak var passwordTxt: UITextField!{
        didSet {
            passwordTxt.tintColor = UIColor.lightGray
            passwordTxt.setIcon(UIImage(named: "password")!)
        }
     }
    
    
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBOutlet weak var exploreBtn: UIButton!
    
    @IBOutlet weak var registerBtn: UIButton!
    
    
    @IBAction func login(_ sender: Any) {
        loginUser()
    }
    
    let defaults = UserDefaults.standard
    
    
    func saveUser(user: User) {
        defaults.set(user.id, forKey: "user_id")
        defaults.set(user.email, forKey: "user_email")
        defaults.set(user.state, forKey: "user_state")
        defaults.set(user.name, forKey: "user_name")
        defaults.set(user.username, forKey: "user_username")
        
        print("Save User \(defaults.integer(forKey: "user_id"))")
    }
    
    
    func loginUser() {
        
        self.loader?.startAnimating()
        
        let body = [
            "phone": phoneTxt.text!,
            "password": passwordTxt.text!,
            ] as [String : Any]
        
        let headers: HTTPHeaders = [
            "Accept": "application/json",
        ]
    
        AF.request("\(host)/login", method: .post,  parameters: body, headers: headers).responseJSON{ [self]response in
            if response.response?.statusCode == 200{
                if let json = response.data {
                    do {
                        let decoder = JSONDecoder()
                        
                        let success = try decoder.decode(User.self, from: json)
                        print(success)
                        saveUser(user: success)
                        performSegue(withIdentifier: "mainStoryboardSegue", sender: nil)
                        
                    } catch let error {
                        let alert = UIAlertController(title: "Erro!", message: "Verifique sua conexão a internet ou volte a tentar mais tarde", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        print(error)
                    }
                }
                self.loader?.stopAnimating()
            }else if response.response?.statusCode == 401{
                let alert = UIAlertController(title: "Erro!", message: "Utilizador ou Senha Incorrectos!", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                self.loader?.stopAnimating()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loader = getLoader()
        btnCustom(loginBtn)
        btnCustomOutline(exploreBtn)
        btnCustomOutline(registerBtn)
    }

}

extension UIViewController{
    
    func getLoader() -> NVActivityIndicatorView{
       let loading = NVActivityIndicatorView(frame: .zero, type: .ballRotateChase, color:  UIColor(named: "color_green"), padding: 0)
       loading.translatesAutoresizingMaskIntoConstraints = false
       
       view.addSubview(loading)
       
       NSLayoutConstraint.activate([
           loading.widthAnchor.constraint(equalToConstant: 40),
           loading.heightAnchor.constraint(equalToConstant: 40),
           loading.centerYAnchor.constraint(equalTo: view.centerYAnchor),
           loading.centerXAnchor.constraint(equalTo: view.centerXAnchor)
       ])
       
        return loading
   }
    
    func hideKeyboardWhenTappedAround() {
          let tap = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
          tap.cancelsTouchesInView = false
          view.addGestureRecognizer(tap)
      }
      
      @objc func dismissKeyboard() {
          view.endEditing(true)
      }
    func keyExist(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    func btnCustom(_ button: UIButton){
        button.layer.cornerRadius = 5
        button.clipsToBounds = true
    }
    
    func makeRoundedImage(_ image: UIImageView) {

      image.layer.borderWidth = 1
      image.layer.masksToBounds = false
        image.layer.borderColor = UIColor.black.cgColor
      image.layer.cornerRadius = image.frame.height/2 //This will change with corners of image and height/2 will make this circle shape
      image.clipsToBounds = true
  }
    
    func btnCustomOutline(_ button: UIButton){
        button.backgroundColor = .clear
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 0.5
        button.layer.borderColor = UIColor.gray.cgColor
        button.clipsToBounds = true
    }
    
    func viewCustom(_ view: UIView){
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
    }
}
extension UITextField {
    func setIcon(_ image: UIImage) {
   let iconView = UIImageView(frame:
                  CGRect(x: 10, y: 5, width: 20, height: 20))
   iconView.image = image
   let iconContainerView: UIView = UIView(frame:
                  CGRect(x: 20, y: 0, width: 30, height: 30))
   iconContainerView.addSubview(iconView)
   leftView = iconContainerView
   leftViewMode = .always
}
}
