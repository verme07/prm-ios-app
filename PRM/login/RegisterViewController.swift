//
//  RegisterViewController.swift
//  PRM
//
//  Created by Johnny on 12/13/20.
//

import UIKit
import Eureka
import Alamofire
import NVActivityIndicatorView


class RegisterViewController: FormViewController {
    
    
    var loader: NVActivityIndicatorView?
    
    
    @IBAction func close(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }
    
    
    var name: String = ""
    var phone: String = ""
    var email: String = ""
    var password: String = ""
    let defaults = UserDefaults.standard
    
    
    func saveUser(user: User) {
        defaults.set(user.id, forKey: "user_id")
        defaults.set(user.email, forKey: "user_email")
        defaults.set(user.state, forKey: "user_state")
        defaults.set(user.name, forKey: "user_name")
        defaults.set(user.username, forKey: "user_username")
    }
    func registerUser() {
        
        loader?.startAnimating()
        let body = [
            "name": name,
            "phone": phone,
            "email": email,
            "password": password,
            ] as [String : Any]
        
        let headers: HTTPHeaders = [
            "Accept": "application/json",
        ]
    
    
        AF.request("\(host)/register", method: .post,  parameters: body, headers: headers).responseJSON{ [self]response in
            if response.response?.statusCode == 200{
                if let json = response.data {
                    do {
                        let decoder = JSONDecoder()
                        
                        let success = try decoder.decode(User.self, from: json)
                        
                        saveUser(user: success)
                        let alert = UIAlertController(title: "Sucesso!", message: "Utilizador registado com sucesso!", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            performSegue(withIdentifier: "registerToMainSegue", sender: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        loader?.stopAnimating()
                        
                    } catch let error {
                        let alert = UIAlertController(title: "Erro!", message: "Verifique sua conexão a internet ou volte a tentar mais tarde", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        loader?.stopAnimating()
                        print(error)
                    }
                }
                
            }else{
                let alert = UIAlertController(title: "Erro!", message: "Verifique sua conexão a internet ou volte a tentar mais tarde", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                loader?.stopAnimating()
            }
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader = getLoader()
        
        form +++ Section("Dodos do Utilizador")
            <<< TextRow(){
                $0.title = "Nome"
                $0.tag = "nome"
                $0.placeholder = "Nome Completo"
                $0.add(rule: RuleRequired(msg: "Insira o Nome"))
                
                $0.validationOptions = .validatesOnChange

            }.cellSetup { cell, row in
                cell.tintColor = .orange
            }
            .cellUpdate { cell, row in
                if !row.isValid {
                    cell.titleLabel?.textColor = .systemRed
                }
                self.name = row.value ?? ""
            }
            <<< EmailRow(){
                $0.title = "Email"
                $0.placeholder = "Email"
                $0.add(rule: RuleRequired(msg: "Insira o Nome"))
                $0.validationOptions = .validatesOnChange
            }.cellSetup { cell, row in
                cell.tintColor = .orange
            }
            .cellUpdate { cell, row in
                if !row.isValid {
                    cell.titleLabel?.textColor = .systemRed
                }
                self.email = row.value ?? ""
            }
            <<< PhoneRow(){
                $0.title = "Celular"
                $0.placeholder = "Numero de Celular"
                $0.add(rule: RuleRequired(msg: "Insira o Nome"))
                $0.validationOptions = .validatesOnChange
            }.cellSetup { cell, row in
                cell.tintColor = .orange
            }
            .cellUpdate { cell, row in
                if !row.isValid {
                    cell.titleLabel?.textColor = .systemRed
                }
                self.phone = row.value ?? ""
            }
            
            +++ Section("Password")
            
            <<< PasswordRow("Senha"){
                $0.title = "Senha"
                $0.add(rule: RuleRequired(msg: "Insira o Nome"))
                
                $0.validationOptions = .validatesOnChange
            }.cellSetup { cell, row in
                cell.tintColor = .orange
            }
            .cellUpdate { cell, row in
                if !row.isValid {
                    cell.titleLabel?.textColor = .systemRed
                }
                
                self.password = row.value ?? ""
            }
       
            <<< ButtonRow() { (row: ButtonRow) -> Void in
                row.title = "Registar"
            }.cellSetup { cell, row in
                cell.backgroundColor = UIColor(named: "color_primary")
                cell.tintColor = .white
                
            }
            .onCellSelection { [weak self] (cell, row) in
//                print("validating errors: \(row.section?.form?.validate().count)")
                if row.section?.form?.validate().count == 0{
                    //
                    self!.registerUser()
                }
            }
  
        // Do any additional setup after loading the view.
    }
    

    
}
