//
//  MarkerWindow.swift
//  PRM
//
//  Created by Johnny on 12/8/20.
//

import UIKit

class MarkerWindow: UIView {

    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var distance: UILabel!
    
    @IBOutlet weak var contact: UILabel!
    
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var btnReport: UIButton!
    
}
