//
//  StationsTableViewCell.swift
//  PRM
//
//  Created by Johnny on 12/27/20.
//

import UIKit

protocol CellDelegate: class {
    func CellBtnTapped(tap: Int)
}

class StationsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var container: UIView!
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var callBtn: UIButton!
    
    @IBOutlet weak var directionsBtn: UIButton!
    
    @IBOutlet weak var distance: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
